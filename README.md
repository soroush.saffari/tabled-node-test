# API Development Test

This project is an exercise for the technical test B17 at Tabled.

### Install Dependencies
1. Install latest version of Node and NPM.
2. Run `npm install`

### Setup Database
Run the following commands:

```
npx knex migrate:latest
npx knex seed:run
```

### Start the Server
Run `npm run server`

- - -

Further instructions to run and complete this exercise will be provided during the interview.