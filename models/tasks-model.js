const db = require('../config/db-config.js')

const readTasks = () => {
  return db('tasks')
}

const readTask = id => {
  // todo: use `db.raw()` to read the task with the given id using native SQL
  return db('tasks').where('id', id)
}

const createTask = task => {
  return db('tasks').insert(task, 'id')
}

const updateTask = (id, task) => {
  // todo: complete method using knex methods
}

module.exports = {
  readTasks,
  readTask,
  createTask,
  updateTask,
}
