const db = require('../config/db-config.js')

const readUsers = () => {
  return db('users')
}

const readUser = id => {
  return db('users').where('id', id)
}

const createUser = user => {
  return db('users').insert(user, 'id')
}

const updateUser = (id, user) => {
  return db('users').where('id', id).update(user)
}

const deleteUser = id => {
  return db('users').where('id', id).del()
}

module.exports = {
  readUsers,
  readUser,
  createUser,
  updateUser,
  deleteUser,
}
