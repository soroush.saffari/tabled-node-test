const request = require('supertest')
const server = require('../server')

const db = require('../config/db-config.js')

beforeAll(async () => {
  await db('tasks').insert([{ title: 'Task A' }, { title: 'Task B' }, { title: 'Task C' }])
})

afterAll(async () => {
  await db.raw(`TRUNCATE TABLE tasks RESTART IDENTITY CASCADE`)
})

describe('tasks endpoints', () => {
  describe('GET /', () => {
    it('should return 200', async () => {
      const response = await request(server).get('/api/tasks').expect(200)
    })

    it('should be an object/array', async () => {
      const response = await request(server).get('/api/tasks').expect(200)
      expect(typeof response.body).toBe('object')
    })

    it('should return a length of 3', async () => {
      const response = await request(server).get('/api/tasks').expect(200)
      expect(response.body.length).toBe(3)
    })
  })

  describe('GET /:id', () => {
    it('should return 200', async () => {
      const response = await request(server).get('/api/tasks/1').expect(200)
    })

    it('should be an object/array', async () => {
      const response = await request(server).get('/api/tasks/1').expect(200)
      expect(typeof response.body).toBe('object')
    })

    it('should return the right user', async () => {
      const expected = { id: 1, title: 'Task A' }
      const response = await request(server).get('/api/tasks/1').expect(200)
      expect(response.body[0].title).toBe(expected.title)
    })
  })

  describe('POST /', () => {
    it('adds a task into db', async () => {
      const user = { title: 'Test Task' }
      const posting = await request(server)
        .post('/api/tasks/')
        .send(user)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(201)

      expect(posting.body.taskId).toEqual([4])
    })

    it('its the right task', async () => {
      const getUser = await request(server).get('/api/tasks/4')
      expect(getUser.body[0].title).toEqual('Test Task')
    })
  })

  describe('PUT /', () => {
    it('changes title of task', async () => {
      const user = { title: 'Test Task Updated' }
      const updating = await request(server)
        .put('/api/tasks/4')
        .send(user)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)

      const getUser = await request(server).get('/api/tasks/4')
      console.log(getUser.body)
    })

    it('its the right task', async () => {
      const getUser = await request(server).get('/api/tasks/4')
      expect(getUser.body[0]).toEqual(undefined)
    })
  })
})
